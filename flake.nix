{
  description = "Dev env";


  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";


  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            zola
            vscode-langservers-extracted
            djlint # Django/Jinja HTML formatter (Tera is similar)
            # djhtml # Django HTML formatter (and Tera)
          ];

          shellHook = ''
            alias serve="zola serve -u localhost --drafts -O"
            serve &
            exec zsh
          '';
        };
      }
    );
}
