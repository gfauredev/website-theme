---
title: "Home"
sort_by: "weight"
extra:
    icon: "fa-solid fas fa-house"
---

<!-- LTeX: language=en -->

# Simple website base

Welcome to my simple zola theme, which stands only as a boilerplate
to get correct, optimized HTML, but as you can see doesn’t provide much style.

## Main goals

- HTML correctness, highly semantic
- Easily CSS-selectable HTML, highly documented
- Flexible generation, good taxonomies support, good multilingual support
- Accessibility
- SEO optimized
- Performance, lightweight
- Providing useful, general-purpose building blocks
  - Shortcodes for integrations (YouTube, Spotify, SoundCloud…)
  - Useful miscellaneous shortcodes like images resizing and tagging
