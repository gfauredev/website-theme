---
title: "Formatting test article"
date: 2022-01-22
description: "Test article with a lot of lorem ipsum"
taxonomies:
    Tags: ["test","Web", "CSS", "HTML"]
---

{{ img(src="https://pixy.org/src/480/4800346.jpg", class="large", alt="Main illustation") }}

# Test article, the first of this blog

This is the first article of this blog.
It will probably talk about nothing, because I currently have nothing to ~~say~~
write.
But it will be useful to test the formatting of my website, particularly with
the following section.

## Random content to test formatting

<!-- more -->

{{ img(src="https://evasion-online.com/images/2015/07/paris-photo.jpg", alt="Second test image", class="right", alt="random image to test formatting") }}

Do you know the legendary lorem ipsum ? This is a nonsense but very long text
which is being used for centuries to test the formatting of a text a is support.
Let’s see what it looks like.

### The great lorem ipsum

Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam voluptatem at amet
tenetur alias explicabo saepe perferendis consectetur ratione, aliquam commodi
maiores. Ducimus, ratione libero ullam eligendi dolorem iste voluptates.
Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam voluptatem at amet
tenetur alias explicabo saepe perferendis consectetur ratione, aliquam commodi
maiores.
{{ yt(id="dQw4w9WgXcQ", class="before") }}

NEED SOME FORMATTING HERE...
Ducimus, ratione libero ullam eligendi dolorem iste voluptates.
Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam voluptatem at amet
tenetur alias explicabo saepe perferendis consectetur ratione, aliquam commodi
maiores. Ducimus, ratione libero ullam eligendi dolorem iste voluptates.
This text is inserted through an emmet snippet, it’s why I only have these 2-3
lines. But don’t worry, it will do the trick. Even more considering that I will
continue copying/pasting it.

### The second great lorem ipsum with amazing snippets of code

{{ img(src="https://www.viptranszfer.hu/wp-content/uploads/2020/09/athen-repuloter-transzfer.jpg", class="large", alt="random image to test formatting") }}

VERY GREAT LANDSCAPE.
Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam voluptatem at amet
tenetur alias explicabo saepe perferendis consectetur ratione, aliquam commodi
maiores. Ducimus, ratione libero ullam eligendi dolorem iste voluptates.
Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam voluptatem at amet
tenetur alias explicabo saepe perferendis consectetur ratione, aliquam commodi
maiores. Ducimus, ratione libero ullam eligendi dolorem iste voluptates.
Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam voluptatem at amet
tenetur alias explicabo saepe perferendis consectetur ratione, aliquam commodi
maiores. Ducimus, ratione libero ullam eligendi dolorem iste voluptates.
This text is inserted through an emmet snippet, it’s why I only have these 2-3
lines. But don’t worry, it will do the trick. Even more considering that I will
continue copying/pasting it.

#### Python code snippet

```python
def loremisator():
    while true:
        print("Lorem ipsum")
    if true == false:
        print("Lorem ipsum is boring")
```

#### C code snippet

```c
void loremisator(void){
    while (true){
        printf("Lorem ipsum");
    }
    if (true == false) {
        printf("Lorem ipsum is very boring");
    }
```

### The Final great lorem ipsum, which for instance has the longest title in ~~the world~~ this website, which is already a pretty good achievement

{{ img(src="https://images.unsplash.com/photo-1473773508845-188df298d2d1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1374&q=80", class="left", alt="random image to test formatting") }}

NEED MARGINS BY HERE.
Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam voluptatem at amet
tenetur alias explicabo saepe perferendis consectetur ratione, aliquam commodi
maiores. Ducimus, ratione libero ullam eligendi dolorem iste voluptates.
Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam voluptatem at amet
tenetur alias explicabo saepe perferendis consectetur ratione, aliquam commodi
maiores. Ducimus, ratione libero ullam eligendi dolorem iste voluptates.

{{ yt(id="e1XHbIwreME", class="after") }}

Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam voluptatem at amet
tenetur alias explicabo saepe perferendis consectetur ratione, aliquam commodi
maiores. Ducimus, ratione libero ullam eligendi dolorem iste voluptates.
This text is inserted through an emmet snippet, it’s why I only have these 2-3
lines. But don’t worry, it will do the trick. Even more considering that I will
continue copying/pasting it. Ffffinally, I’m at the bottom of the viewport !
Not too soon !

## A bit of relaxing music

{{ yt(id="n61ULEU7CO0") }}

# Final level 1 header to test TOC

Pretty sad but you already finished reading this super interesting article.
