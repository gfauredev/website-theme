# Guilhem Fauré’s simple website theme

> Development has moved on [Neutral Zola Theme](https://github.com/gfauredev/Neutral-Zola-Theme)

My humble static website theme for [Zola](getzola.org),
in HTML (Tera) templates.

The main goals are :

- HTML correctness
- Highly semantic, easily CSS-selectable HTML
- Accessibility
- SEO
- Performance
- Providing useful, general-purpose building blocks

This theme is not intended to be beautiful, probably not intended
for direct use, but rather as a basis that do the generic work.
